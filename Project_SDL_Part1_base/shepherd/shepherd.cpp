#include "../Project_SDL1.h"

shepherd::shepherd(const std::string& filePath, SDL_Surface* windowSurfacePtr)
    : moving_object(filePath, windowSurfacePtr) {

  this->offset = 0;
  this->x = frame_boundary + (rand() % (frame_width-frame_boundary*2-this->getWidth()*2)) + this->getWidth();
  this->y = frame_boundary + (rand() % (frame_height-frame_boundary*2-this->getHeight()*2)) + this->getHeight();
  this->set_attribute("player");
}
void shepherd::move() {
  draw();
}
