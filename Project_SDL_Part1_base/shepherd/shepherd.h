#ifndef PROJECT_SDL1_SHEPHERD_H
#define PROJECT_SDL1_SHEPHERD_H

class shepherd: public moving_object {
protected:
  int offset = 200;
public:
  shepherd(const std::string& filePath, SDL_Surface* windowSurfacePtr);
  void move() override;
};

#endif // PROJECT_SDL1_SHEPHERD_H
