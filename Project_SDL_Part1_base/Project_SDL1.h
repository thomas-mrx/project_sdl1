﻿// SDL_Test.h: Includedatei für Include-Standardsystemdateien
// oder projektspezifische Includedateien.

#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <climits>
#include <cmath>
#include <experimental/optional>
#include <iostream>
#include <map>
#include <memory>
#include <numeric>
#include <unordered_map>
#include <vector>

#include "engine/application.h"
#include "engine/ground.h"
#include "engine/moving_object.h"

#include "shepherd/shepherd.h"

#include "animal/animal.h"
#include "animal/sheep.h"
#include "animal/wolf.h"
#include "animal/shepherd_dog.h"


// Defintions
constexpr double frame_rate = 60.0; // refresh rate
constexpr double frame_time = 1. / frame_rate;
constexpr unsigned frame_width = 1400; // Width of window in pixel
constexpr unsigned frame_height = 900; // Height of window in pixel
// Minimal distance of animals to the border
// of the screen
constexpr unsigned frame_boundary = 100;

// Helper function to initialize SDL
void init();

typedef struct coord {
  int x, y;
} coord;

static Uint32 coordsToUint32(int x, int y) {
  return (x << 16 | y);
}

static coord uint32toCoords(Uint32 coords) {
  int x = coords >> 16;
  int y = coords & 0x0000FFFF;
  return coord{x,y};
}

static double vectorAverage(std::vector<int> const& v) {
  return 1.0 * std::accumulate(v.begin(), v.end(), 0) / v.size();
}