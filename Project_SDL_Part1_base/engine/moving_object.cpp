#include "../Project_SDL1.h"
#include "moving_object.h"


moving_object::moving_object(const std::string& file_path, SDL_Surface* window_surface_ptr) {
  
  this->window_surface_ptr_ = window_surface_ptr;

  const char *file = file_path.c_str();
  this->image_ptr_ = IMG_Load(file);

  if (!this->image_ptr_)
    throw std::runtime_error("Could not load image");

  std::string path = file_path;

  std::string flip_path = path.replace(path.end()-4,path.end(),"_flip.png");
  const char *file_flip = flip_path.c_str();
  this->image_flip_ptr_ = IMG_Load(file_flip);

  std::string dead_path = path.replace(path.end()-9,path.end(),"_dead.png");
  const char *file_dead = dead_path.c_str();
  this->image_dead_ptr_ = IMG_Load(file_dead);

  this->x = 0;
  this->y = 0;
  this->random = (rand()%10000);
}

moving_object::~moving_object() noexcept {
  SDL_FreeSurface(this->image_ptr_);
  if(this->image_flip_ptr_)
    SDL_FreeSurface(this->image_flip_ptr_);
}


void moving_object::draw() {
  int width = this->getWidth();
  int height = this->getHeight();
  if (this->has_attribute("baby")) {
    Uint32 sec = ((this->has_attribute("dead") ? this->get_attribute("dead") : SDL_GetTicks()) - this->get_attribute("baby"));
    int stepW = (this->getWidth()/2) * (((double)sec / 10000));
    int stepH = (this->getHeight()/2) * (((double)sec / 10000));
    width = (this->getWidth()/2)+stepW;
    height = (this->getHeight()/2)+stepH;
    if(sec >= 10000){
      this->remove_attribute("baby");
    }
  }
  SDL_Surface *surface = nullptr;
  if(this->has_attribute("dead") && this->image_dead_ptr_) {
    surface = this->image_dead_ptr_;
  } else if (this->flipped && this->image_flip_ptr_) {
    surface = this->image_flip_ptr_;
  } else {
    surface = this->image_ptr_;
  }
  auto scaledRect = SDL_Rect{0,0,width,height};
  SDL_Surface *scaledImg = SDL_CreateRGBSurface(0,width,height,32, 0xff, 0xff00, 0xff0000, 0xff000000);
  SDL_BlitScaled( surface, NULL, scaledImg, &scaledRect );

  auto rect = SDL_Rect{this->x,this->y,width,height};
  if (SDL_BlitSurface(scaledImg, nullptr, this->window_surface_ptr_, &rect))
    throw std::runtime_error("Could not apply texture.");

}

int moving_object::getX() {
  return this->x;
}

int moving_object::getY() {
  return this->y;
}

int moving_object::getDirectionX() {
  return this->dx;
}

int moving_object::getDirectionY() {
  return this->dy;
}

int moving_object::getWidth() {
  return this->image_ptr_->w;
}

int moving_object::getHeight() {
  return this->image_ptr_->h;
}

void moving_object::reverse_direction(moving_object* m, int step) {
  this->dx = ((m->dx > 0) ? -1 : 1)*step;
  this->dy = ((m->dy > 0) ? -1 : 1)*step;
  this->flipped = (this->dx < 0);
}

void moving_object::set_position(int x, int y, bool direction) {
  this->x = x;
  this->y = y;
  this->flipped = direction;
}

void moving_object::set_attribute(const std::string& key, Uint32 value) {
  this->attributes[key] = value;
}

Uint32 moving_object::get_attribute(const std::string& key) {
  return this->attributes[key];
}

void moving_object::remove_attribute(const std::string& key) {
  this->attributes.erase(key);
}

bool moving_object::has_attribute(const std::string& key) {
  return this->attributes.find(key) != this->attributes.end();
}