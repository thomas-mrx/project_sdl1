#include "../Project_SDL1.h"

#ifndef PROJECT_SDL1_MOVING_OBJECT_H
#define PROJECT_SDL1_MOVING_OBJECT_H
class moving_object {
private:
  SDL_Surface* window_surface_ptr_; // ptr to the surface on which we want the
                                          // animal to be drawn, also non-owning
  SDL_Surface* image_ptr_; // The texture of the sheep (the loaded image), use
  SDL_Surface* image_flip_ptr_ = nullptr;
  SDL_Surface* image_dead_ptr_ = nullptr;

protected:
  int random, x, y, dx = 1, dy = 1;
  int offset = 200;
  bool flipped = false;
  std::unordered_map<std::string, Uint32> attributes;
public:
  moving_object(const std::string& file_path, SDL_Surface* window_surface_ptr);
  // todo: The constructor has to load the sdl_surface that corresponds to the
  // texture
  ~moving_object(); // todo: Use the destructor to release memory and "clean up
                    // behind you"

  void draw(); // todo: Draw the animal on the screen <-> window_surface_ptr.
               // Note that this function is not virtual, it does not depend
               // on the static type of the instance

  virtual void move() = 0; // todo: animals move around, but in a different
                           // fashion depending on which type of animal
  int getX();
  int getY();
  int getWidth();
  int getHeight();
  void set_position(int x, int y, bool direction);
  bool has_attribute(const std::string& key);
  void set_attribute(const std::string& key, Uint32 value = 0);
  Uint32 get_attribute(const std::string& key);
  void remove_attribute(const std::string& key);
  void reverse_direction(moving_object* m, int step);
  int getDirectionX();
  int getDirectionY();
};

#endif // PROJECT_SDL1_MOVING_OBJECT_H
