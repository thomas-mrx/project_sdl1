#include "moving_object.h"

#ifndef PROJECT_SDL1_GROUND_H
#define PROJECT_SDL1_GROUND_H

class ground {
private:
  SDL_Surface* window_surface_ptr_;
  TTF_Font* font_ptr_;

  std::vector<std::unique_ptr<moving_object>> moving_objects;
  std::vector<int> score;
  void offspring(moving_object* m);

public:
  Uint32 start;
  unsigned period;

  ground(SDL_Surface* window_surface_ptr, TTF_Font* font_ptr);
  ~ground();
  void draw_score();
  void add_moving_object(moving_object* m);
  bool update();
  void mousePress(SDL_MouseButtonEvent event);
  void keyPress(SDL_Keycode i);
  void defend(moving_object* m);
  void attack(moving_object* m);
  void init_score(int n_prey);
  void credits();
};

#endif // PROJECT_SDL1_GROUND_H

