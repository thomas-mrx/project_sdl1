
#ifndef PROJECT_SDL1_APPLICATION_H
#define PROJECT_SDL1_APPLICATION_H
#include "ground.h"
class application {
private:
  SDL_Window* window_ptr_;
  SDL_Surface* window_surface_ptr_;
  TTF_Font*  font_ptr_;
  SDL_Event window_event_;

  ground* g;


public:
  application(unsigned n_sheep, unsigned n_wolf);
  ~application();

  int loop(unsigned period);
};
#endif // PROJECT_SDL1_APPLICATION_H
