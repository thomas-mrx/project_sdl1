#include "../Project_SDL1.h"
#include "ground.h"

ground::ground(SDL_Surface* window_surface_ptr, TTF_Font*  font_ptr) {
  this->window_surface_ptr_ = window_surface_ptr;
  this->font_ptr_ = font_ptr;
}

ground::~ground(){
  for (auto &m : this->moving_objects)
  {
    std::cout << "destruct object@" << m.get() << "\n";
    m.reset();
  }
  std::cout << "destruct ground@" << this << "\n";
  SDL_FreeSurface(this->window_surface_ptr_);
}

void ground::add_moving_object(moving_object *m){
  this->moving_objects.emplace_back(m);
}

void ground::draw_score(){
  unsigned total_time = period - ((SDL_GetTicks() - start) / 1000);
  unsigned minutes = floor(total_time / 60);
  unsigned seconds = total_time % 60;
  char text[25];
  sprintf(text, "%02d:%02d - Score : %.1f", minutes, seconds, vectorAverage(score));

  SDL_Color White = {255, 255, 255};
  SDL_Surface* surfaceMessage = TTF_RenderText_Solid(this->font_ptr_, text, White);
  auto rect = SDL_Rect{frame_boundary + 8,28,256,100};

  SDL_BlitSurface(surfaceMessage, nullptr, this->window_surface_ptr_, &rect);
}

void ground::init_score(int n_prey) {
  score.push_back(n_prey);
}

bool ground::update(){
  const SDL_Rect rect = SDL_Rect{0,0,frame_width,frame_height};
  const SDL_Rect rect2 = SDL_Rect{frame_boundary,frame_boundary,frame_width-frame_boundary*2,frame_height-frame_boundary*2};
  SDL_FillRect(this->window_surface_ptr_, &rect,  SDL_MapRGB(this->window_surface_ptr_->format, 86, 125, 70));
  SDL_FillRect(this->window_surface_ptr_, &rect2,  SDL_MapRGB(this->window_surface_ptr_->format, 84, 123, 68));
  draw_score();

  int currentScore = 0;
  bool hasPrey = false;
  bool hasHunter = false;
  for (auto it = moving_objects.begin(); it != moving_objects.end();) {
    if (it->get()->has_attribute("dead") && it->get()->get_attribute("dead") + 2000 < SDL_GetTicks()) {
      it = moving_objects.erase(it);
    } else {
      it->get()->move();
      if(it->get()->has_attribute("hunter")) {
        this->attack(it->get());
      } else if (it->get()->has_attribute("prey")) {
        this->offspring(it->get());
      } else if (it->get()->has_attribute("defender")) {
        this->defend(it->get());
      }
      it->get()->draw();
      ++it;
    }
    if(it != moving_objects.end()) {
      if(it->get()->has_attribute("prey")) {
        hasPrey = true;
        currentScore++;
      } else if(it->get()->has_attribute("hunter")) {
        hasHunter = true;
      }
    }
  }
  int lastScore = score.back();
  if(currentScore != lastScore) {
    score.push_back(currentScore);
  }

  if (!hasHunter || !hasPrey) {
    return true;
  }

  //tweak to redraw the shepherd in front of animals
  if (!moving_objects.empty()) {
    auto &shepherd = moving_objects.at(0);
    shepherd->draw();
  }
  //end of tweak

  return false;
}

void ground::offspring(moving_object* m) {
  if (!m->has_attribute("runaway")) {
    for (auto &npc : moving_objects){
      if (m->has_attribute("sheep") && npc->has_attribute("sheep")
          && !m->has_attribute("offspring") && !npc->has_attribute("offspring")
          && !m->has_attribute("baby") && !npc->has_attribute("baby")) {
        if((m->has_attribute("male") && npc->has_attribute("female"))
            || (npc->has_attribute("male") && m->has_attribute("female"))){
          int distance = sqrt(pow(m->getX() - npc->getX(), 2) + pow(m->getY() - npc->getY(), 2) * 1.0);
          if (distance < m->getWidth()/2 && distance < m->getHeight()/2) {
            if(m->has_attribute("female")){
              m->set_attribute("offspring", SDL_GetTicks());
            } else {
              npc->set_attribute("offspring", SDL_GetTicks());
            }
            sheep *s = new sheep("media/sheep.png", this->window_surface_ptr_, m->getX(), m->getY());
            s->set_attribute("baby", SDL_GetTicks());
            this->add_moving_object(s);
            break;
          }
        }
      }
    }
  }
}

void ground::defend(moving_object* m) {
    for (auto &npc : moving_objects){
      if (!npc->has_attribute("runaway") && npc->has_attribute("hunter") ) {
        int distance = sqrt(pow(m->getX() - npc->getX(), 2) + pow(m->getY() - npc->getY(), 2) * 1.0);
        if (distance < m->getWidth()*4 && distance < m->getHeight()*4) {
          npc->set_attribute("runaway", SDL_GetTicks());
          npc->reverse_direction(m, 2);
          break;
        }
      }
    }
}

void ground::attack(moving_object* m) {
  if(!m->has_attribute("dead")) {
    int closest = INT_MAX;
    moving_object *closest_prey = nullptr;
    for (auto &npc : moving_objects){
      if (npc->has_attribute("prey") && !npc->has_attribute("dead")) {
        int distance = sqrt(pow(npc->getX() - m->getX(), 2) + pow(npc->getY() - m->getY(), 2) * 1.0);
        if (distance < npc->getWidth()/2 && distance < npc->getHeight()/2) {
          npc->set_attribute("dead", SDL_GetTicks());
          m->set_attribute("eat", SDL_GetTicks());
        } else if (distance < npc->getWidth()*2 && distance < npc->getHeight()*2 && !npc->has_attribute("runaway")) {
          npc->set_attribute("runaway", SDL_GetTicks());
          npc->reverse_direction(m, 5);
        } else if (distance < closest) {
          closest = distance;
          closest_prey = npc.get();
        }
      }
    }
    if(closest_prey && !m->has_attribute("runaway")){
      int divide = closest / 3;
      if (divide == 0) {
        divide = 1;
      }
      m->set_position(m->getX()+((closest_prey->getX()-m->getX())/divide),m->getY()+((closest_prey->getY()-m->getY())/divide), (closest_prey->getX()-m->getX() < 0));
    } else {
      m->set_position(m->getX()+m->getDirectionX(),m->getY()+m->getDirectionY(), (m->getDirectionX() < 0));
    }
  }
}

void ground::mousePress(SDL_MouseButtonEvent event) {
  if(event.button == SDL_BUTTON_LEFT) {
    for (auto &a : moving_objects){
      if(a->has_attribute("shepherd_dog")) {
        if(event.x < (frame_width - frame_boundary - a->getWidth()/2)
            && event.x >= (frame_boundary)
            && event.y < (frame_height - frame_boundary - a->getHeight()/2)
            && event.y >= (frame_boundary)) {
          a->set_attribute("dog_fetch", coordsToUint32(event.x, event.y));
        }
      }
    }
  }
}
void ground::keyPress(SDL_Keycode i) {
  for (auto &a : moving_objects){
    if(a->has_attribute("player")) {
      int step = 5;
      int x = a->getX();
      int y = a->getY();
      if(i == SDLK_RIGHT) {
        x += step;
      } else if(i == SDLK_LEFT) {
        x -= step;
      } else if(i == SDLK_UP) {
        y -= step;
      } else if(i == SDLK_DOWN) {
        y += step;
      }
      if(x >= (frame_width - frame_boundary - a->getWidth()) || x < (frame_boundary)  ) {
        x = a->getX();
      }
      if(y >= (frame_height - frame_boundary - a->getHeight()) || y < (frame_boundary)  ) {
        y = a->getY();
      }
      a->set_position(x,y,(a->getX()-x < 0));
    }
  }
}
void ground::credits() {
  const SDL_Rect rect = SDL_Rect{0,0,frame_width,frame_height};
  SDL_FillRect(this->window_surface_ptr_, &rect,  SDL_MapRGB(this->window_surface_ptr_->format, 86, 125, 70));


  SDL_Color White = {255, 255, 255};

  SDL_Surface* surfaceMessage = TTF_RenderText_Solid(this->font_ptr_, "The end !!", White);
  auto end_rect = SDL_Rect{(this->window_surface_ptr_->w/2)-96,(this->window_surface_ptr_->h/2)-50,256,100};
  SDL_BlitSurface(surfaceMessage, nullptr, this->window_surface_ptr_, &end_rect);

  char text[18];
  sprintf(text, "Your score is %.1f", vectorAverage(score));
  SDL_Surface* surfaceScore = TTF_RenderText_Solid(this->font_ptr_, text, White);
  auto text_rect = SDL_Rect{(this->window_surface_ptr_->w/2)-160,this->window_surface_ptr_->h/2,256,100};
  SDL_BlitSurface(surfaceScore, nullptr, this->window_surface_ptr_, &text_rect);

  SDL_Surface* surfaceCredits = TTF_RenderText_Solid(this->font_ptr_, "by Tony BERNIS, Romain GAUVREAU & Thomas MOREAUX", White);
  auto credits_rect = SDL_Rect{(this->window_surface_ptr_->w/2)-128,this->window_surface_ptr_->h-50,256,100};
  SDL_BlitSurface(surfaceCredits, nullptr, this->window_surface_ptr_, &credits_rect);
}
