#include "../Project_SDL1.h"

/*
 * SDL_Window* window_ptr_;
 * SDL_Surface* window_surface_ptr_;
 * SDL_Event window_event_;
 */

application::application(unsigned n_sheep, unsigned n_wolf) {
  this->window_ptr_ = SDL_CreateWindow("SDL2 Window",
                                     SDL_WINDOWPOS_CENTERED,
                                     SDL_WINDOWPOS_CENTERED,
                                       frame_width, frame_height,
                                     0);

  if (!this->window_ptr_)
    throw std::runtime_error(std::string(SDL_GetError()));

  this->window_surface_ptr_ = SDL_GetWindowSurface(this->window_ptr_);

  if (!this->window_surface_ptr_)
    throw std::runtime_error(std::string(SDL_GetError()));

  TTF_Init();
  this->font_ptr_ = TTF_OpenFont("media/VT323-Regular.ttf", 42);

  if (!this->font_ptr_)
    throw std::runtime_error(std::string(SDL_GetError()));

  this->g = new ground(this->window_surface_ptr_, this->font_ptr_);

  shepherd *s = new shepherd("media/shepherd.png", this->window_surface_ptr_);
  g->add_moving_object(s);

  shepherd_dog *d = new shepherd_dog("media/doggo.png",this->window_surface_ptr_, s);
  g->add_moving_object(d);

  for(int i = 0; i < n_sheep; ++i) {
    sheep *s = new sheep("media/sheep.png", this->window_surface_ptr_);
    g->add_moving_object(s);
  }
  for(int i = 0; i < n_wolf; ++i) {
    wolf *w = new wolf("media/wolf.png", this->window_surface_ptr_);
    g->add_moving_object(w);
  }

  this->g->init_score(n_sheep);

}
application::~application() {
  delete this->g;
  std::cout << "destruct app@" << this << "\n";
  SDL_FreeSurface(this->window_surface_ptr_);
  SDL_DestroyWindow(this->window_ptr_);
}



int application::loop(unsigned period) {

  auto start = SDL_GetTicks();
  this->g->start = start;
  this->g->period = period;

  bool end = false;
  while (!end && (SDL_GetTicks() - start < period*1000)) {
    auto tick = SDL_GetTicks();
    end = this->g->update();
    SDL_UpdateWindowSurface(this->window_ptr_);
    while(SDL_GetTicks() < tick+(frame_time*1000));
    while (SDL_PollEvent(&this->window_event_)) {
      switch(this->window_event_.type){
        case SDL_QUIT:
          end = true;
          break;
        case SDL_MOUSEBUTTONDOWN:
          g->mousePress(this->window_event_.button);
          break;
        case SDL_KEYDOWN:
          g->keyPress(this->window_event_.key.keysym.sym);
          break;
      }
    }
  }
  bool quit = false;
  while (!quit) {
    auto tick = SDL_GetTicks();
    this->g->credits();
    SDL_UpdateWindowSurface(this->window_ptr_);
    while(SDL_GetTicks() < tick+(frame_time*1000));
    while (SDL_PollEvent(&this->window_event_)) {
      if(this->window_event_.type == SDL_QUIT) {
        quit = true;
      }
    }
  }

  return 0;
}
