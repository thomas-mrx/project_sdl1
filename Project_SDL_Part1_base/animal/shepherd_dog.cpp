#include "../Project_SDL1.h"

shepherd_dog::shepherd_dog(const std::string& filePath,SDL_Surface* windowSurfacePtr, class shepherd *s)
    : animal(filePath,windowSurfacePtr) {
  this->shepherd = s;
  this->offset = 100;
  this->multiplier = (100 + (rand() % 150)) /100;
  this->clock = ((rand() % 100) > 50)?-1:1;
  this->set_attribute("shepherd_dog");
  this->set_attribute("defender");
}

void shepherd_dog::move() {
  int time = (int)SDL_GetTicks() + this->random;
  int expectedX = ( (this->shepherd->getX()+(this->shepherd->getWidth()/2)) + (int)(cos(this->clock * time * M_PI * 2 * 0.0001 * this->multiplier) * this->offset) ) - this->getWidth()/M_PI;
  int expectedY = ( (this->shepherd->getY()+(this->shepherd->getHeight()/2)) + (int)(sin(this->clock * time * M_PI *2 * 0.0001 * this->multiplier) * this->offset) ) - this->getHeight()/M_PI;
  if(this->has_attribute("dog_fetch")){
    Uint32 coords = this->get_attribute("dog_fetch");
    coord c = uint32toCoords(coords);
    int goalX = c.x;
    int goalY = c.y;
    int distance = sqrt(pow(goalX - this->x, 2) + pow(goalY - this->y, 2) * 1.0);
    int divide = distance / 5;
    if (divide == 0) {
      divide = 1;
    }
    this->set_position(this->x+((goalX-this->x)/divide),this->y+((goalY-this->y)/divide), (this->x < this->x+((goalX-this->x)/divide)));
    if (this->x == expectedX && this->y == expectedY) {
      this->remove_attribute("dog_fetch");
    } else if (this->x == goalX && this->y == goalY) {
      this->set_attribute("dog_fetch", coordsToUint32(expectedX, expectedY));
    }
  } else {
    this->x = expectedX;
    this->y = expectedY;
  }
}
