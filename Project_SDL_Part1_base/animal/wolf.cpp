#include "../Project_SDL1.h"

wolf::wolf(const std::string& filePath, SDL_Surface* windowSurfacePtr)
    : animal(filePath,windowSurfacePtr) {

  //loup ctor
  //this->offset = (rand() % 50);
  this->dx = ((rand() % 100) > 50)?-2:2;
  this->flipped = !(dx > 0);
  this->dy = ((rand() % 100) > 50)?-2:2;
  this->x = frame_boundary + (rand() % (frame_width-frame_boundary*2-this->getWidth()*2)) + this->getWidth();
  this->y = frame_boundary + (rand() % (frame_height-frame_boundary*2-this->getHeight()*2)) + this->getHeight();

  this->set_attribute("hunter");
  this->set_attribute("wolf");
  this->set_attribute("eat", SDL_GetTicks());

}

void wolf::move() {
  if(this->x >= (frame_width - frame_boundary - this->getWidth()) || this->x < (frame_boundary)  ) {
    this->dx = -this->dx;
    this->flipped = !this->flipped;
  }

  if(this->y >= (frame_height - frame_boundary - this->getHeight()) || this->y < (frame_boundary)  ) {
    this->dy = -this->dy;
  }

  if(!this->has_attribute("dead") && this->get_attribute("eat") + 15000 < SDL_GetTicks()) {
    this->set_attribute("dead", SDL_GetTicks());
  }

  if(this->has_attribute("runaway") && this->get_attribute("runaway") + 1000 < SDL_GetTicks()) {
    this->dx /= 2;
    this->dy /= 2;
    this->remove_attribute("runaway");
  }


}


