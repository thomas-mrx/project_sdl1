#include "../Project_SDL1.h"

sheep::sheep(const std::string& filePath, SDL_Surface* windowSurfacePtr, int x, int y)
    : animal(filePath,windowSurfacePtr) {

  //mouton ctor
  /*this->offset = (rand() % 175) + 25;
  this->multiplier = (100 + (rand() % 150)) /100;
  this->clock = ((rand() % 100) > 50)?-1:1;*/
  this->dx = ((rand() % 100) > 50)?-1:1;
  this->dy = ((rand() % 100) > 50)?-1:1;
  this->x = x == 0 ? (frame_boundary + (rand() % (frame_width-frame_boundary*2-this->getWidth()*2)) + this->getWidth()) : x;
  this->y = y == 0 ? (frame_boundary + (rand() % (frame_height-frame_boundary*2-this->getHeight()*2)) + this->getHeight()) : y;

  std::string sex = ((rand() % 100) > 50)?"female":"male";
  this->set_attribute(sex);
  this->set_attribute("prey");
  this->set_attribute("sheep");
}


void sheep::move() {
  if(this->x >= (frame_width - frame_boundary - this->getWidth()) || this->x < (frame_boundary)  ) {
    this->dx = -this->dx;
  }

  if(this->y >= (frame_height - frame_boundary - this->getHeight()) || this->y < (frame_boundary)  ) {
    this->dy = -this->dy;
  }
  if(!this->has_attribute("dead")) {
    this->x += this->dx;
    this->y += this->dy;
  }
  if(this->has_attribute("offspring") && this->get_attribute("offspring") + 15000 < SDL_GetTicks()) {
    this->remove_attribute("offspring");
  }

  if(this->has_attribute("runaway") && this->get_attribute("runaway") + 2000 < SDL_GetTicks()) {
    this->dx /= 5;
    this->dy /= 5;
    this->remove_attribute("runaway");
  }
}


