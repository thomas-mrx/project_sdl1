#ifndef PROJECT_SDL1_WOLF_H
#define PROJECT_SDL1_WOLF_H
class wolf : public animal {
public:
  wolf(const std::string& filePath, SDL_Surface* windowSurfacePtr);


  void move() override;
};
#endif // PROJECT_SDL1_WOLF_H
