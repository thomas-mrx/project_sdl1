#ifndef PROJECT_SDL1_SHEPHERD_DOG_H
#define PROJECT_SDL1_SHEPHERD_DOG_H

class shepherd_dog : public animal {
private:
  double multiplier = 1.0;
  int clock;
  shepherd* shepherd;
public:
  shepherd_dog(const std::string& filePath, SDL_Surface* windowSurfacePtr,
               class shepherd* s = nullptr);
  void move() override;
};

#endif // PROJECT_SDL1_SHEPHERD_DOG_H
