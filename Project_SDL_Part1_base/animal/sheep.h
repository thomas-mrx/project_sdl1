#ifndef PROJECT_SDL1_SHEEP_H
#define PROJECT_SDL1_SHEEP_H
class sheep : public animal {
private:
  double multiplier = 1.0;
  int clock;
public:
  sheep(const std::string& filePath, SDL_Surface* windowSurfacePtr, int x = 0, int y = 0);
  void move() override;
};
#endif // PROJECT_SDL1_SHEEP_H

