#ifndef PROJECT_SDL1_ANIMAL_H
#define PROJECT_SDL1_ANIMAL_H
class animal : public moving_object {
protected:
  int offset = 200;
public:
  animal(const std::string& filePath, SDL_Surface* windowSurfacePtr);
};
#endif // PROJECT_SDL1_ANIMAL_H

